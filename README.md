# Vinland Solutions CommonCore.Text Library

[![License](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/license/mit.svg)](https://opensource.org/licenses/MIT)
[![Platform](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/platform/netstandard/2.0_2.1.svg)](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
[![Repository](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/repository.svg)](https://gitlab.com/commoncorelibs/commoncore-text)
[![Releases](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/releases.svg)](https://gitlab.com/commoncorelibs/commoncore-text/-/releases)
[![Documentation](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/documentation.svg)](https://commoncorelibs.gitlab.io/commoncore-text/)
[![Nuget](https://badgen.net/nuget/v/VinlandSolutions.CommonCore.Text/latest?icon)](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Text/)
[![Pipeline](https://gitlab.com/commoncorelibs/commoncore-text/badges/master/pipeline.svg)](https://gitlab.com/commoncorelibs/commoncore-text/commits/master)
[![Coverage](https://gitlab.com/commoncorelibs/commoncore-text/badges/master/coverage.svg)](https://commoncorelibs.gitlab.io/commoncore-text/reports/index.html)

**NOTE: This project is in ![alpha](https://gitlab.com/commoncorelibs/pipeline/-/raw/master/badge/stage/alpha.svg) development and the public api is extremely unstable. Breaking changes are possible with each pre-release.**

**CommonCore.Text** is a .Net Standard 2.0/2.1 library ?.

## Installation

The official release versions of the **CommonCore.Text** library are hosted on [NuGet](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Text/) and can be installed using the standard console means, or found through the Visual Studio NuGet Package Managers.

This library is compatible with projects targeting at least .Net Framework 4.6.1, .Net Core 2.0, or .Net 5.

## Usage

?

## Projects

The **CommonCore.Text** repository is composed of three projects with the listed dependencies:

* **CommonCore.Text**: The dotnet standard 2.0/2.1 library project.
  * [CommonCore.Exceptions](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Exceptions/)
  * [CommonCore.Shims](https://www.nuget.org/packages/VinlandSolutions.CommonCore.Shims/) (dotnet standard 2.0 target)
* **CommonCore.Text.Docs**: The project for generating api documentation.
  * [DocFX](https://github.com/dotnet/docfx)
* **CommonCore.Text.Tests**: The project for running unit tests and generating coverage reports.
  * [xUnit](https://github.com/xunit/xunit)
  * [FluentAssertions](https://github.com/fluentassertions/fluentassertions)

## Links

- Repository: https://gitlab.com/commoncorelibs/commoncore-text
- Issues: https://gitlab.com/commoncorelibs/commoncore-text/issues
- Docs: https://commoncorelibs.gitlab.io/commoncore-text/index.html
- Nuget: https://www.nuget.org/packages/VinlandSolutions.CommonCore.Text/

## Credits

?
