﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace CommonCore.Text
{
    public readonly partial struct SubString
    {
        public static SubString Empty { get; } = new(null);

        public SubString(string? source) : this(source, 0, source?.Length ?? 0) { }

        public SubString(string? source, int index) : this(source, index, source is null || index >= source.Length ? 0 : source.Length - index) { }

        public SubString(string? source, int index, int length)
        {
            int sourceLength = source?.Length ?? 0;
            Throw.If.Arg.Index.Invalid(nameof(index), index, sourceLength, true);
            int maxLength = sourceLength - index;
            Throw.If.Arg.Range.Outside(nameof(length), length, 0, maxLength, true, true);
            this.Source = source ?? string.Empty;
            this.Index = index;
            this.Length = length;
        }

        public readonly string Source { get; }

        public readonly int Index { get; }

        public readonly int Length { get; }

        public readonly bool IsEmpty => this.Length == 0;

        public readonly char this[int index]
        {
            get
            {
                Throw.If.Arg.Index.Invalid(nameof(index), index, this.AsSpan());
                return this.Source[this.Index + index];
            }
        }

        public readonly ReadOnlySpan<char> AsSpan()
            => this.Source.AsSpan(this.Index, this.Length);

        public readonly ReadOnlyMemory<char> AsMemory()
            => this.Source.AsMemory(this.Index, this.Length);

        public readonly override string ToString()
            => this.IsEmpty ? string.Empty : this.Source.Substring(this.Index, this.Length);

        [ExcludeFromCodeCoverage]
        public static implicit operator ReadOnlySpan<char>(SubString substring)
            => substring.AsSpan();

        [ExcludeFromCodeCoverage]
        public static implicit operator SubString(string source)
            => new(source);
    }

    public readonly partial struct SubString
    {
        public SubString Slice(int startIndex)
            => new(this.Source, this.Index + startIndex);

        public SubString Slice(int startIndex, int length)
            => new(this.Source, this.Index + startIndex, length);
    }

    public readonly partial struct SubString : IEquatable<SubString>
    {
        [ExcludeFromCodeCoverage]
        public override int GetHashCode()
            => SubStringEqualityComparer.Default.GetHashCode(this);

        public bool Equals(object? obj)
            => obj is SubString other && SubStringEqualityComparer.Default.Equals(this, other);

        public bool Equals(SubString other)
            => SubStringEqualityComparer.Default.Equals(this, other);

        public static bool operator ==(SubString left, SubString right)
            => SubStringEqualityComparer.Default.Equals(left, right);

        public static bool operator !=(SubString left, SubString right)
            => !SubStringEqualityComparer.Default.Equals(left, right);
    }

    public readonly partial struct SubString : IComparable, IComparable<SubString>
    {
        public int CompareTo(object? obj)
            => obj is SubString other ? SubStringComparer.Default.Compare(this, other) : 0;

        public int CompareTo(SubString other)
            => SubStringComparer.Default.Compare(this, other);

        public static bool operator >(SubString left, SubString right)
            => SubStringComparer.Default.Compare(left, right) > 0;

        public static bool operator <(SubString left, SubString right)
            => SubStringComparer.Default.Compare(left, right) < 0;

        public static bool operator >=(SubString left, SubString right)
            => SubStringComparer.Default.Compare(left, right) >= 0;

        public static bool operator <=(SubString left, SubString right)
            => SubStringComparer.Default.Compare(left, right) <= 0;
    }

    public readonly partial struct SubString : IEnumerable<char>
    {
        public IEnumerator<char> GetEnumerator()
        {
            for (int index = 0; index < this.Length && index + this.Index < this.Source.Length; index++)
            {
                yield return this.Source[index + this.Index];
            }
        }

        [ExcludeFromCodeCoverage]
        IEnumerator IEnumerable.GetEnumerator()
            => ((IEnumerable<char>) this).GetEnumerator();
    }

    public class SubStringComparer : Comparer<SubString>
    {
        public static new SubStringComparer Default { get; } = new SubStringComparer();

        public override int Compare(SubString left, SubString right)
        {
            // SequenceCompareTo can throw values outside -1, 0, and 1, so need to clamp it.
            int result = left.AsSpan().SequenceCompareTo(right.AsSpan());
            return result < -1 ? -1 : result > 1 ? 1 : result;
        }
    }

    public class SubStringEqualityComparer : EqualityComparer<SubString>
    {
        public static new SubStringEqualityComparer Default { get; } = new SubStringEqualityComparer();

        [ExcludeFromCodeCoverage]
        public override int GetHashCode(SubString token)
            => HashCode.Combine(token.Source, token.Index, token.Length);

        public override bool Equals(SubString left, SubString right)
            => left.AsSpan().SequenceEqual(right.AsSpan());
    }

    public class SubStringFormatter
    {
        public static SubStringFormatter Default { get; } = new SubStringFormatter();

        public string ToString(SubString token)
            => token.AsSpan().ToString();
    }

    public static partial class SubStringExtensions
    {
    }
}
