﻿#region Copyright (c) 2022 Jay Jeckel
// Copyright (c) 2022 Jay Jeckel
// Licensed under the MIT license: https://opensource.org/licenses/MIT
// Permission is granted to use, copy, modify, and distribute the work.
// Full license information available in the project LICENSE file.
#endregion

#nullable enable

using System;
using System.Globalization;
using System.Linq;

namespace CommonCore.Text.Numeric
{
    /// <summary>
    /// The <see cref="NumericTextHelper"/> <c>static</c> <c>helper</c> <c>class</c>
    /// provides limited functionality to equate, compare, and increment strings as
    /// if they were numbers. Currently all methods assume provided input contains
    /// only digits (ie, 0123456789), so negative numbers are not support.
    /// </summary>
    public static class NumericTextHelper
    {
        private static bool All<T>(this ReadOnlySpan<T> self, Func<T, bool> func)
        {
            if (self.IsEmpty) { return false; }
            for (int index = 0; index < self.Length; index++)
            { if (!func(self[index])) { return false; } }
            return true;
        }

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> is a dash symbol
        /// (-);
        /// otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a dash symbol; otherwise <c>false</c>.</returns>
        public static bool IsDash(char value) => value is '-';

        #region IsAlphabetic

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> is a lower or upper cased letter
        /// (abcdefghijklmnopqrstuvwxyz or ABCDEFGHIJKLMNOPQRSTUVWXYZ);
        /// otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a lower or upper cased letter; otherwise <c>false</c>.</returns>
        public static bool IsAlphabetic(char value) => value is (>= 'a' and <= 'z') or (>= 'A' and <= 'Z');

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> contains only letters; otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The value to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> contains only letters; otherwise <c>false</c>.</returns>
        public static bool IsAlphabetic(ReadOnlySpan<char> value) => value.All(c => IsAlphabetic(c));

        /// <inheritdoc cref="IsAlphabetic(ReadOnlySpan{char})"/>
        public static bool IsAlphabetic(string? value) => IsAlphabetic(value.AsSpan());

        #endregion

        #region IsNumeric

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> is a numeric digit
        /// (0123456789);
        /// otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a digit; otherwise <c>false</c>.</returns>
        public static bool IsNumeric(char value) => value is >= '0' and <= '9';

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> contains only digits; otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The value to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> contains only digits; otherwise <c>false</c>.</returns>
        public static bool IsNumeric(ReadOnlySpan<char> value) => value.All(c => IsNumeric(c));

        /// <inheritdoc cref="IsNumeric(ReadOnlySpan{char})"/>
        public static bool IsNumeric(string? value)=> IsNumeric(value.AsSpan());

        #endregion

        #region IsAlphaNumeric

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> is a letter or digit
        /// (abcdefghijklmnopqrstuvwxyz or ABCDEFGHIJKLMNOPQRSTUVWXYZ or 0123456789);
        /// otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The character to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> is a hyphen, letter, or digit; otherwise <c>false</c>.</returns>
        public static bool IsAlphaNumeric(char value) => IsNumeric(value) || IsAlphabetic(value);

        /// <summary>
        /// Returns <c>true</c> if <paramref name="value"/> contains only digits or letters; otherwise <c>false</c>.
        /// </summary>
        /// <param name="value">The value to compare.</param>
        /// <returns><c>true</c> if <paramref name="value"/> contains only digits or letters; otherwise <c>false</c>.</returns>
        public static bool IsAlphaNumeric(ReadOnlySpan<char> value) => value.All(c => IsAlphaNumeric(c));

        /// <inheritdoc cref="IsAlphaNumeric(ReadOnlySpan{char})"/>
        public static bool IsAlphaNumeric(string? value) => IsAlphaNumeric(value.AsSpan());

        #endregion

        #region IsAlphaNumeric

        /// <summary>
        /// Counts the number of sequential zeros that start the specified <paramref name="value"/>.
        /// The last character is never counted since even if it is zero it is not a LEADING zero.
        /// </summary>
        /// <param name="value">The value to process.</param>
        /// <returns>The number of sequential zeros that start the specified <paramref name="value"/>.</returns>
        public static int CountLeadingZeros(ReadOnlySpan<char> value)
        {
            if (value.Length <= 1) { return 0; }

            int count = 0;
            for (int index = 0; index < value.Length - 1; index++)
            { if (value[index] == '0') { count++; } else { break; } }
            return count;
        }

        /// <inheritdoc cref="CountLeadingZeros(ReadOnlySpan{char})"/>
        public static int CountLeadingZeros(string? value)
            => CountLeadingZeros(value.AsSpan());

        #endregion

        #region Equate

        /// <summary>
        /// Compares two char sequences for equality as if they were numeric values,
        /// ignoring LEADING zeros.
        /// </summary>
        /// <param name="left">The digits to compare to the other.</param>
        /// <param name="right">The digits to be compared against.</param>
        /// <returns><c>true</c> if char sequences are numerically equivalent; otherwise <c>false</c>.</returns>
        public static bool Equate(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
        {
            if (left.IsEmpty || right.IsEmpty) { return left.IsEmpty == right.IsEmpty; }
            var spanLeft = left.Slice(NumericTextHelper.CountLeadingZeros(left));
            var spanRight = right.Slice(NumericTextHelper.CountLeadingZeros(right));
            return spanLeft.SequenceEqual(spanRight);
        }

        /// <inheritdoc cref="Equate(ReadOnlySpan{char}, ReadOnlySpan{char})"/>
        public static bool Equate(string? left, string? right)
            => Equate(left.AsSpan(), right.AsSpan());

        #endregion

        #region Compare

        /// <summary>
        /// Compares two char sequences as if they were numeric values, returning <c>-1</c>, <c>0</c>, or <c>1</c>.
        /// As the method name suggests, it is assumed that the specified values are comprised of only digit characters.
        /// The method does not perform any actual conversion to a numeric type, so theoretically there is
        /// no limit to the size of the numbers.
        /// </summary>
        /// <param name="left">The first digits to be compared.</param>
        /// <param name="right">The other digits to be compared.</param>
        /// <returns>
        /// <c>-1</c> if left is less than right;
        /// <c>1</c> if left is greater than right;
        /// otherwise <c>0</c> if left and right are equal.
        /// </returns>
        public static int Compare(ReadOnlySpan<char> left, ReadOnlySpan<char> right)
        {
            // Handle any empty spans, empty lower than non-empty.
            if (left.IsEmpty && right.IsEmpty) { return 0; }
            else if (left.IsEmpty) { return -1; }
            else if (right.IsEmpty) { return 1; }

            // To approximate numeric comparison, trim any LEADING zeros,
            // but do NOT trim the last character even if it is a zero.
            var spanLeft = left.Slice(NumericTextHelper.CountLeadingZeros(left));
            var spanRight = right.Slice(NumericTextHelper.CountLeadingZeros(right));

            int result;
            // Shorter numbers are less than longer, so lower.
            if (spanLeft.Length < spanRight.Length) { result = -1; }
            // Longer numbers are greater than shorter, so higher.
            else if (spanLeft.Length > spanRight.Length) { result = 1; }
            // Equal length numbers are compared by sequence.
            else
            {
                // SequenceCompareTo can throw values outside -1, 0, and 1, so need to clamp it.
                result = spanLeft.SequenceCompareTo(spanRight);
                result = result < -1 ? -1 : result > 1 ? 1 : result;
            }

            // If values are numerically equal, but one of them originally had leading zeros,
            // then which is lower precedence depends on the order they are compared, leading to
            // inconsistent results. To have fully deterministic results, we have to choose one
            // as lower than the other.
            // The shorter original value is considered lower.
            if (result == 0 && left.Length < right.Length) { result = -1; }
            else if (result == 0 && left.Length > right.Length) { result = 1; }

            //Console.WriteLine($"{left} ({spanLeft.Length}) | {right} ({spanRight.Length}) | {result}");
            return result;
        }

        /// <inheritdoc cref="Compare(ReadOnlySpan{char}, ReadOnlySpan{char})"/>
        public static int Compare(string? left, string? right)
            => Compare(left.AsSpan(), right.AsSpan());

        #endregion

        #region Increment

        /// <summary>
        /// Returns a new numeric <see cref="string"/> representing the result of incrementing the original numeric string.
        /// The result is equivalent to converting <paramref name="value"/> to a number, incrementing it by one,
        /// and converting the result back to a string.
        /// As the method name suggests, it is assumed that the specified <paramref name="value"/> is comprised
        /// of only digit characters.
        /// The method does not perform any actual conversion to a numeric type, so theoretically there is
        /// no limit to the size of the number.
        /// </summary>
        /// <param name="value">The value to increment.</param>
        /// <returns>A new numeric string representing the result of incrementing the original numeric string.</returns>
        public static string Increment(ReadOnlySpan<char> value)
        {
            // Empty becomes '0', conceptually this is an empty token being created.
            if (value.IsEmpty) { return "0"; }

            // Find the first char from end of value that is NOT nine;
            // otherwise -1 if all nines.
            int position = -1;
            for (int index = value.Length - 1; index > -1; index--)
            { if (value[index] != '9') { position = index; break; } }

            Span<char> dest;
            if (position == -1)
            {
                // If no non-nine is found, then dest length is value length plus one.
                // Set first dest char to '1' and the rest to '0'.
                dest = new(new char[value.Length + 1]);
                dest.Fill('0');
                dest[0] = '1';
            }
            else
            {
                // Copy value chars before the target position to dest,
                // then set target position dest char to incremented value char,
                // then set dest chars after the target position to '0'.
                dest = new(new char[value.Length]);
                value.Slice(0, position).CopyTo(dest);
                dest[position] = (char) (value[position] + 1);
                dest.Slice(position + 1).Fill('0');
            }
            return dest.ToString();
        }

        /// <inheritdoc cref="Increment(ReadOnlySpan{char})"/>
        public static string Increment(string? value)
            => Increment(value.AsSpan());

        #endregion

        /// <summary>
        /// Tries to parse the specified <paramref name="value"/> to a <see cref="int"/> if possible;
        /// otherwise the specified <paramref name="fallback"/> is returned.
        /// The specified <paramref name="value"/> is parsed according to
        /// <see cref="NumberStyles.None"/> and the <see cref="CultureInfo.NumberFormat"/> of the
        /// <see cref="CultureInfo.InvariantCulture"/>. This effectively means that only strings
        /// containing exclusively digit characters will be parsed.
        /// </summary>
        /// <param name="value">The semantic token to parse.</param>
        /// <param name="fallback">The value to return if parsing fails.</param>
        /// <returns>A parsed integer; otherwise the <paramref name="fallback"/> value.</returns>
        public static int ParseNumber(string? value, int fallback = 0)
            => value is not null && int.TryParse(value, NumberStyles.None, CultureInfo.InvariantCulture.NumberFormat, out int number)
            ? number : fallback;

    }
}
